#!/usr/bin/env bash
# Fix missing application commands symbolic links in the /snap/bin
# directory
#
# Copyright 2023 林博仁(Buo-ren, Lin) <buo.ren.lin@gmail.com>
# SPDX-License-Identifier: CC-BY-SA-4.0

SNAPD_SOCKET="${SNAPD_SOCKET:-/run/snapd.socket}"

set_opts=(
    # Terminate script execution when an unhandled error occurs
    -o errexit
    -o errtrace

    # Terminate script execution when an unset parameter variable is
    # referenced
    -o nounset
)
if ! set "${set_opts[@]}"; then
    printf \
        'Error: Unable to set the defensive interpreter behavior.\n' \
        1>&2
    exit 1
fi

required_commands=(
    curl
    jq
)
flag_dependency_check_failed=false
for command in "${required_commands[@]}"; do
    if ! command -v "${command}" >/dev/null; then
        flag_dependency_check_failed=true
        printf \
            'Error: Unable to locate the "%s" command in the command search PATHs.\n' \
            "${command}" \
            1>&2
    fi
done
if test "${flag_dependency_check_failed}" == true; then
    printf \
        'Error: Dependency check failed, please check your installation.\n' \
        1>&2
    exit 1
fi

if test "${EUID}" -ne 0; then
    printf \
        'Error: This program requires to be run as the superuser(root).\n' \
        1>&2
    exit 1
fi

printf \
    'Info: Checking whether the snapd socket is available...\n'
if ! test -e "${SNAPD_SOCKET}"; then
    printf \
        'Error: Unable to locate the snapd runtime communication socket(%s).\n' \
        "${SNAPD_SOCKET}" \
        1>&2
    exit 1
fi

printf \
    'Info: Querying the list of the installed snaps from the snapd service...\n'
curl_opts_common=(
    --unix-socket "${SNAPD_SOCKET}"

    # Don't show progress report messages, keep error output, though
    --silent
    --show-error
)
if ! installed_snaps_raw="$(
    curl "${curl_opts_common[@]}" http://localhost/v2/snaps
    )"; then
    printf \
        'Error: Unable to query the list of the installed snaps from the snapd service.\n' \
        1>&2
    exit 2
fi

if test "$(
    jq \
        --raw-output \
        .type \
        <<<"${installed_snaps_raw}"
    )" == error; then
    if ! error_message="$(
        jq \
            --raw-output \
            .result.message \
            <<<"${installed_snaps_raw}"
        )"; then
        error_message='Unable to query the error description from snapd'
    fi

    printf \
        'Error: Unable to query the list of the installed snaps from snapd service: %s\n' \
        "${error_message}" \
        1>&2
    exit 2
fi

if ! installed_snaps_newline_separated="$(
    jq --raw-output .result[].name <<<"${installed_snaps_raw}"
    )"; then
    printf \
        'Error: Unable to parse out the installed snap list from the snapd service API response.\n' \
        1>&2
    exit 2
fi

if ! mapfile -t installed_snaps \
    <<<"${installed_snaps_newline_separated}"; then
    printf \
        'Error: Unable to store the installed snaps list into a Bash array.\n' \
        1>&2
    exit 2
fi

# These snaps may need to be taken care first
# https://gitlab.com/brlin/fix-snap-bin/-/issues/3
base_snaps_regex='^(core[[:digit:]]*)$'
base_snaps=()
for snap in "${installed_snaps[@]}"; do
    if [[ "${snap}" =~ ${base_snaps_regex} ]]; then
        base_snaps+=("${snap}")
    fi
done
for snap in "${base_snaps[@]}"; do
    printf \
        'Info: Querying the details of the "%s" snap from the snapd service API...\n' \
        "${snap}"
    if ! snap_details_raw="$(
        curl \
            "${curl_opts_common[@]}" \
            "http://localhost/v2/snaps/${snap}"
        )"; then
        printf \
            'Error: Unable to query the details of the "%s" snap from the snapd service API.\n' \
            "${snap}" \
            1>&2
        exit 2
    fi

    printf \
        'Info: Querying the currently installed revision of the "%s" snap...\n' \
        "${snap}"
    if ! snap_revision="$(
        jq \
            --raw-output \
            .result.revision \
            <<<"${snap_details_raw}"
        )"; then
        printf \
            'Error: Unable to parse out the currently installed revision of the "%s" snap from the snapd API response.\n' \
            "${snap}" \
            1>&2
        exit 2
    fi
    printf \
        'Info: The currently installed revision of the "%s" snap is "%s".\n' \
        "${snap}" \
        "${snap_revision}"

    printf \
        'Info: Submitting the refresh request of the "%s" revision of the "%s" snap...\n' \
        "${snap_revision}" \
        "${snap}"
    curl_opts=(
        "${curl_opts_common[@]}"
        --request POST
        --data "{
            \"action\": \"refresh\",
            \"revision\": \"${snap_revision}\"
        }"
    )
    if ! snap_refresh_raw="$(
        curl \
            "${curl_opts[@]}" \
            "http://localhost/v2/snaps/${snap}"
        )"; then
        printf \
            'Error: Error occurred when submitting the refresh request of the "%s" revision of the "%s" snap.\n' \
            "${snap_revision}" \
            "${snap}" \
            1>&2
        exit 2
    fi

    if test "$(
        jq \
            --raw-output \
            .type \
            <<<"${snap_refresh_raw}"
        )" == error; then
        if ! error_message="$(
            jq \
                --raw-output \
                .result.message \
                <<<"${snap_refresh_raw}"
            )"; then
            error_message="Unable to query the error description from the snapd API's response."
        fi

        printf \
            'Error: Unable to submit the refresh request of the "%s" revision of the "%s" snap: %s\n' \
            "${snap_revision}" \
            "${snap}" \
            "${error_message}" \
            1>&2
        exit 2
    fi

    printf \
        'Info: Refresh request submitted for the "%s" revision of the "%s" snap.\n' \
        "${snap_revision}" \
        "${snap}"

    if ! snapd_change_id="$(
        jq \
            --raw-output \
            .change \
            <<<"${snap_refresh_raw}"
        )"; then
        printf \
            'Error: Unable to parse out the snapd change identifier from the snapd API response.\n' \
            1>&2
        exit 2
    fi

    printf \
        'Info: Checking whether the refresh operation completed for the "%s" revision of the "%s" snap...' \
        "${snap_revision}" \
        "${snap}"
    while true; do
        if ! snapd_change_details_raw="$(
            curl \
                "${curl_opts_common[@]}" \
                "http://localhost/v2/changes/${snapd_change_id}"
            )"; then
            printf \
                '\nError: Unable to query the details of the "%s" snapd change.\n' \
                "${snapd_change_id}" \
                1>&2
            exit 2
        fi

        if test "$(
            jq \
                --raw-output \
                .type \
                <<<"${snapd_change_details_raw}"
            )" == error; then
            if ! error_message="$(
                jq \
                    --raw-output \
                    .result.message \
                    <<<"${snapd_change_details_raw}"
                )"; then
                error_message="Unable to query the error description from the snapd API's response."
            fi

            printf \
                '\nError: Unable to check the current status of the "%s" snapd change: %s\n' \
                "${snapd_change_id}" \
                "${error_message}" \
                1>&2
            exit 2
        fi

        if ! snapd_change_ready="$(
            jq .result.ready <<<"${snapd_change_details_raw}"
            )"; then
            printf \
                '\nError: Unable to parse out the ready property of the snapd change "%s".\n' \
                "${snapd_change_id}" \
                1>&2
            exit 2
        fi

        if test "${snapd_change_ready}" == true; then
            printf 'done.\n'
            break
        fi

        # Operation continuation ellipsis dot
        printf '.'

        sleep 1
    done

    if ! snapd_change_status="$(
        jq --raw-output .result.status <<<"${snapd_change_details_raw}"
        )"; then
        printf \
            'Error: Unable to parse out the status property of the snapd change "%s".\n' \
            "${snapd_change_id}" \
            1>&2
        exit 2
    fi

    if test "${snapd_change_status}" != Done; then
        if ! snapd_change_last_task_summary="$(
            jq \
                --raw-output \
                .result.tasks[-1].summary \
                <<<"${snapd_change_details_raw}"
            )"; then
            printf \
                "Error: Unable to parse out the last task's summary text of the snapd change \"%s\".\\n" \
                "${snapd_change_id}" \
                1>&2
            exit 2
        fi

        printf \
            'Error: Unable to refresh the "%s" revision of the "%s" snap during the task "%s", refer the output of the "sudo snap change %s" command for more information.\n' \
            "${snap_revision}" \
            "${snap}" \
            "${snapd_change_last_task_summary}" \
            "${snapd_change_id}" \
            1>&2
        exit 2
    fi
done

refresh_failed_snaps=()
for snap in "${installed_snaps[@]}"; do
    if [[ "${snap}" =~ ${base_snaps_regex} ]]; then
        continue
    fi

    printf \
        'Info: Querying the details of the "%s" snap from the snapd service API...\n' \
        "${snap}"
    if ! snap_details_raw="$(
        curl \
            "${curl_opts_common[@]}" \
            "http://localhost/v2/snaps/${snap}"
        )"; then
        printf \
            'Error: Unable to query the details of the "%s" snap from the snapd service API.\n' \
            "${snap}" \
            1>&2
        exit 2
    fi

    if test "$(
        jq \
            --raw-output \
            .type \
            <<<"${snap_details_raw}"
        )" == error; then
        if ! error_message="$(
            jq \
                --raw-output \
                .result.message \
                <<<"${snap_details_raw}"
            )"; then
            error_message="Unable to query the error description from the snapd API's response."
        fi

        printf \
            'Error: Unable to query the details of the "%s" snap from the snapd service: %s\n' \
            "${snap}" \
            "${error_message}" \
            1>&2
        exit 2
    fi

    printf \
        'Info: Checking whether the "%s" snap has published any application commands...\n' \
        "${snap}"
    if test "$(jq .result.apps <<<"${snap_details_raw}")" == null; then
        printf \
            "Info: The \"%s\" snap doesn't publish any application commands, skipping...\\n" \
            "${snap}"
        continue
    else
        printf \
            'Info: Detected that the "%s" snap has published application commands and should be refreshed.\n' \
            "${snap}"
    fi

    printf \
        'Info: Querying the currently installed revision of the "%s" snap...\n' \
        "${snap}"
    if ! snap_revision="$(
        jq \
            --raw-output \
            .result.revision \
            <<<"${snap_details_raw}"
        )"; then
        printf \
            'Error: Unable to parse out the currently installed revision of the "%s" snap from the snapd API response.\n' \
            "${snap}" \
            1>&2
        exit 2
    fi
    printf \
        'Info: The currently installed revision of the "%s" snap is "%s".\n' \
        "${snap}" \
        "${snap_revision}"

    printf \
        'Info: Submitting the refresh request of the "%s" revision of the "%s" snap...\n' \
        "${snap_revision}" \
        "${snap}"
    curl_opts=(
        "${curl_opts_common[@]}"
        --request POST
        --data "{
            \"action\": \"refresh\",
            \"revision\": \"${snap_revision}\"
        }"
    )
    if ! snap_refresh_raw="$(
        curl \
            "${curl_opts[@]}" \
            "http://localhost/v2/snaps/${snap}"
        )"; then
        printf \
            'Error: Error occurred when submitting the refresh request of the "%s" revision of the "%s" snap.\n' \
            "${snap_revision}" \
            "${snap}" \
            1>&2
        exit 2
    fi

    if test "$(
        jq \
            --raw-output \
            .type \
            <<<"${snap_refresh_raw}"
        )" == error; then
        if ! error_message="$(
            jq \
                --raw-output \
                .result.message \
                <<<"${snap_refresh_raw}"
            )"; then
            error_message="Unable to query the error description from the snapd API's response."
        fi

        printf \
            'Error: Unable to submit the refresh request of the "%s" revision of the "%s" snap: %s\n' \
            "${snap_revision}" \
            "${snap}" \
            "${error_message}" \
            1>&2
        refresh_failed_snaps+=("${snap}")
        continue
    fi

    printf \
        'Info: Refresh request submitted for the "%s" revision of the "%s" snap.\n' \
        "${snap_revision}" \
        "${snap}"

    if ! snapd_change_id="$(
        jq \
            --raw-output \
            .change \
            <<<"${snap_refresh_raw}"
        )"; then
        printf \
            'Error: Unable to parse out the snapd change identifier from the snapd API response.\n' \
            1>&2
        exit 2
    fi

    printf \
        'Info: Checking whether the refresh operation completed for the "%s" revision of the "%s" snap...' \
        "${snap_revision}" \
        "${snap}"
    while true; do
        if ! snapd_change_details_raw="$(
            curl \
                "${curl_opts_common[@]}" \
                "http://localhost/v2/changes/${snapd_change_id}"
            )"; then
            printf \
                '\nError: Unable to query the details of the "%s" snapd change.\n' \
                "${snapd_change_id}" \
                1>&2
            exit 2
        fi

        if test "$(
            jq \
                --raw-output \
                .type \
                <<<"${snapd_change_details_raw}"
            )" == error; then
            if ! error_message="$(
                jq \
                    --raw-output \
                    .result.message \
                    <<<"${snapd_change_details_raw}"
                )"; then
                error_message="Unable to query the error description from the snapd API's response."
            fi

            printf \
                '\nError: Unable to check the current status of the "%s" snapd change: %s\n' \
                "${snapd_change_id}" \
                "${error_message}" \
                1>&2
            exit 2
        fi

        if ! snapd_change_ready="$(
            jq .result.ready <<<"${snapd_change_details_raw}"
            )"; then
            printf \
                '\nError: Unable to parse out the ready property of the snapd change "%s".\n' \
                "${snapd_change_id}" \
                1>&2
            exit 2
        fi

        if test "${snapd_change_ready}" == true; then
            printf 'done.\n'
            break
        fi

        # Operation continuation ellipsis dot
        printf '.'

        sleep 1
    done

    if ! snapd_change_status="$(
        jq --raw-output .result.status <<<"${snapd_change_details_raw}"
        )"; then
        printf \
            'Error: Unable to parse out the status property of the snapd change "%s".\n' \
            "${snapd_change_id}" \
            1>&2
        exit 2
    fi

    if test "${snapd_change_status}" != Done; then
        if ! snapd_change_last_task_summary="$(
            jq \
                --raw-output \
                .result.tasks[-1].summary \
                <<<"${snapd_change_details_raw}"
            )"; then
            printf \
                "Error: Unable to parse out the last task's summary text of the snapd change \"%s\".\\n" \
                "${snapd_change_id}" \
                1>&2
            exit 2
        fi

        printf \
            'Error: Unable to refresh the "%s" revision of the "%s" snap during the task "%s", refer the output of the "sudo snap change %s" command for more information.\n' \
            "${snap_revision}" \
            "${snap}" \
            "${snapd_change_last_task_summary}" \
            "${snapd_change_id}" \
            1>&2
        refresh_failed_snaps+=("${snap}")
    fi
done

if test "${#refresh_failed_snaps[@]}" -ne 0; then
    printf \
        'Warning: Operation completed, but the following snap refresh operation failed:\n\n'

    for snap in "${refresh_failed_snaps[@]}"; do
        printf '* %s\n' "${snap}"
    done

    printf '\n'
else
    printf \
        'Info: Operation completed without errors.\n'
fi
