# Fix /snap/bin

Fix missing snap application command symbolic links in the /snap/bin directory

<https://gitlab.com/brlin/fix-snap-bin>  
[![The GitLab CI pipeline status badge of the project's `main` branch](https://gitlab.com/brlin/fix-snap-bin/badges/main/pipeline.svg?ignore_skipped=true "Click here to check out the comprehensive status of the GitLab CI pipelines")](https://gitlab.com/brlin/fix-snap-bin/-/pipelines) [![pre-commit enabled badge](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white "This project uses pre-commit to check potential problems")](https://pre-commit.com/) [![REUSE Specification compliance badge](https://api.reuse.software/badge/gitlab.com/brlin/fix-snap-bin "This project complies to the REUSE specification to decrease software licensing costs")](https://api.reuse.software/info/gitlab.com/brlin/fix-snap-bin)

## Features

* Don't download any new snaps from the internet, only refreshing what's already installed in the system
* Uses the v2 snapd RESTful API for the operation, which response format should be stable in the long term
* Will bail out when encounting any errors, preventing much harm to the system

## Prerequisites

* You need to have the superuser(root) privilege on the system to be fixed
* The system to be fixed may need to have internet access for the snapd service to function correctly
* You need to have the following software installed and have its executables in your command search PATHs:
    + [curl](https://curl.se/)  
      For interacting with the snapd service
    + [jq](https://jqlang.github.io/jq/)  
      For parsing the snapd service REST API response

## How to use

1. Download the release archive from [the Releases page](https://gitlab.com/brlin/fix-snap-bin/-/releases)
1. Extract the downloaded release archive
1. Ensure that the [fix-snap-bin.sh](fix-snap-bin.sh) utility program in the extracted product folder has the executable(x) Unix file permission
1. Launch a text terminal application
1. Run the [fix-snap-bin.sh](fix-snap-bin.sh) utility program as root:

    ```bash
    sudo /path/to/fix-snap-bin.sh
    ```

   You may need to run the program multiple times if it encounters any problems(like the snap has processes still being run)

## Environment variables to customize the utility's behavior

### SNAPD_SOCKET

For customizing the snapd runtime communication socket(if it's not in the upstream default path)

**Default value:** `/run/snapd.socket`

## References

* [Using the REST API | Snapcraft documentation](https://snapcraft.io/docs/using-the-api)  
  Introduction article on how to interact with the snapd REST API
* [Snapd REST API | Snapcraft documentation](https://snapcraft.io/docs/snapd-api)  
  The complete reference of the snapd REST API
* [Bug #2085850 “Please support a way to repair/rebuild the /snap/bin directory.” : Bugs : snapd package : Ubuntu](https://bugs.launchpad.net/ubuntu/+source/snapd/+bug/2085850)  
  Upstream bug report to avoid this problem.

## Licensing

Unless otherwise noted(individual file's header/[REUSE DEP5](.reuse/dep5)), this product is licensed under [the 4.0 International version of the Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/4.0/), or any of its more recent versions of your preference.

This work complies to the [REUSE Specification](https://reuse.software/spec/), refer [REUSE - Make licensing easy for everyone](https://reuse.software/) for info regarding the licensing of this product.
